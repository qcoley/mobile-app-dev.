package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import cs.mad.flashcards.FlashDatabase
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashDao
import cs.mad.flashcards.entities.FlashcardSet
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val flashDao by lazy { FlashDatabase.getSetDatabase(applicationContext).FlashDao() }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.flashcardSetList.adapter = FlashcardSetAdapter(listOf(), flashDao)

        loadCards()
        setButtons() }


    private fun loadCards() {
        lifecycleScope.launch {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).setData(flashDao.getSets())

        } }


    // I messed around with this for a few hours, not sure whats causing the crash on click :(
    private fun setButtons() {
        binding.createSetButton.setOnClickListener {
            lifecycleScope.launch {
            ( binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem("test")

            } } } }