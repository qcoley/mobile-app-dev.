package cs.mad.flashcards

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import cs.mad.flashcards.entities.getHardcodedFlashcards

class StudySetActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_set)

        val list = getHardcodedFlashcards()
        val myCards = ArrayList(list)

        val oneQuestion: TextView = findViewById(R.id.card1quest)
        val oneAnswer: TextView = findViewById(R.id.card1answer)


        // get study cards from detail activity -----------------------------------------------
        val studyBun = intent.extras
        val cq1: String?; val ca1: String?; val cq2: String?; val ca2: String?
        val cq3: String?; val ca3: String?; val cq4: String?; val ca4: String?
        val cq5: String?; val ca5: String?; val cq6: String?; val ca6: String?
        val cq7: String?; val ca7: String?; val cq8: String?; val ca8: String?
        val cq9: String?; val ca9: String?; val cq10: String?; val ca10: String?


        if (studyBun != null) { cq1 = studyBun.getString("card1Question", myCards[0].question)
            oneQuestion.text = cq1 }

        if (studyBun != null) { ca1 = studyBun.getString("card1Answer", myCards[0].answer)
            oneAnswer.text = ca1 }

        //------------------------------------------------------------------------------------------
        if (studyBun != null) { cq2 = studyBun.getString("card2Question", myCards[1].question)
            myCards[1].question = cq2 }

        if (studyBun != null) { ca2 = studyBun.getString("card2Answer", myCards[1].answer)
            myCards[1].answer = ca2 }

        //------------------------------------------------------------------------------------------
        if (studyBun != null) { cq3 = studyBun.getString("card3Question", myCards[2].question)
            myCards[2].question = cq3 }

        if (studyBun != null) { ca3 = studyBun.getString("card3Answer", myCards[2].answer)
            myCards[2].answer = ca3 }

        //------------------------------------------------------------------------------------------
        if (studyBun != null) { cq4 = studyBun.getString("card4Question", myCards[3].question)
            myCards[3].question = cq4 }

        if (studyBun != null) { ca4 = studyBun.getString("card4Answer", myCards[3].answer)
            myCards[3].answer = ca4 }

        //------------------------------------------------------------------------------------------
        if (studyBun != null) { cq5 = studyBun.getString("card5Question", myCards[4].question)
            myCards[4].question = cq5 }

        if (studyBun != null) { ca5 = studyBun.getString("card5Answer", myCards[4].answer)
            myCards[4].answer = ca5 }

        //------------------------------------------------------------------------------------------
        if (studyBun != null) { cq6 = studyBun.getString("card6Question", myCards[5].question)
            myCards[5].question = cq6 }

        if (studyBun != null) { ca6 = studyBun.getString("card6Answer", myCards[5].answer)
            myCards[5].answer = ca6 }

        //------------------------------------------------------------------------------------------
        if (studyBun != null) { cq7 = studyBun.getString("card7Question", myCards[6].question)
            myCards[6].question = cq7 }

        if (studyBun != null) { ca7 = studyBun.getString("card7Answer", myCards[6].answer)
            myCards[6].answer = ca7 }

        //------------------------------------------------------------------------------------------
        if (studyBun != null) { cq8 = studyBun.getString("card8Question", myCards[7].question)
            myCards[7].question = cq8 }

        if (studyBun != null) { ca8 = studyBun.getString("card8Answer", myCards[7].answer)
            myCards[7].answer = ca8 }

        //------------------------------------------------------------------------------------------
        if (studyBun != null) { cq9 = studyBun.getString("card9Question", myCards[8].question)
            myCards[8].question = cq9 }

        if (studyBun != null) { ca9 = studyBun.getString("card9Answer", myCards[8].answer)
            myCards[8].answer = ca9 }

        //------------------------------------------------------------------------------------------
        if (studyBun != null) { cq10 = studyBun.getString("card10Question", myCards[9].question)
            myCards[9].question = cq10 }

        if (studyBun != null) { ca10 = studyBun.getString("card10Answer", myCards[9].answer)
            myCards[9].answer = ca10 }



        // skip, missed and correct buttons still to do (sorry I just didn't have time ): )
        val skipButton = findViewById<Button>(R.id.skipped)
        skipButton.setOnClickListener { //to do
        }

        val missButton = findViewById<Button>(R.id.missed)
        missButton.setOnClickListener { //to do
        }

        val correctButton = findViewById<Button>(R.id.correct)
        correctButton.setOnClickListener { // to do
        }

        // -----------------------------------------------------------------------------------------


        // to keep track of card side for flip button
        var flipCount = 0

        // flip card to see answer
        val flipperButton = findViewById<Button>(R.id.flipper)
        flipperButton.setOnClickListener {

            // if card has not been flipped, show answer on click
            if (flipCount % 2 == 0){ flipCount ++

            oneQuestion.visibility = View.INVISIBLE
            oneAnswer.visibility = View.VISIBLE }

            // if card has been flipped, show question on click
            else { flipCount ++
                oneAnswer.visibility = View.INVISIBLE
                oneQuestion.visibility = View.VISIBLE     }      }


        // return to main screen
        val doneStudyButton = findViewById<Button>(R.id.reDone)
        doneStudyButton.setOnClickListener {

            val doneIntent = Intent(this, DetailedCardActivity::class.java)
            startActivity(doneIntent)     }     }     }