package cs.mad.flashcards

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardAdapter
import cs.mad.flashcards.entities.getHardcodedFlashcards
import kotlin.random.Random


class DetailedCardActivity : AppCompatActivity(), FlashcardAdapter.OnItemClickListener {

    private val myCards = generateCards(200)
    private val myAdapter = FlashcardAdapter(myCards, this)


    // when a card is clicked on -------------------------------------------------------------------
    override fun onItemClick(position: Int) {

        Toast.makeText(this,"Card Opened", Toast.LENGTH_SHORT).show()
        myAdapter.notifyItemChanged(position)

        val question = myCards[position].question
        val answer = myCards[position].answer

        // bundle original card question and answer and send to edit screen
        val originBun = Bundle()
        originBun.putString("originQuestion", question)
        originBun.putString("originAnswer", answer)
        originBun.putInt("originPosition", position)

        val originIntent = Intent(this, FlashCardDialog::class.java)
        originIntent.putExtras(originBun)

        startActivity(originIntent)     }


    // when activity starts ------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.detailedcardactivity)
        val recyclerView =findViewById<RecyclerView>(R.id.recycler_card)

        recyclerView.adapter = myAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)


        // buttons to navigate
        val addCardButton = findViewById<Button>(R.id.addCard)
        addCardButton.setOnClickListener {

            val index:Int = 10
            val newCard = Flashcard("New Question", "New Answer")
            myCards.add(index, newCard)
            myAdapter.notifyItemInserted(index) }

        val removeCardButton = findViewById<Button>(R.id.removeCard)
        removeCardButton.setOnClickListener {

            val index:Int = 10
            myCards.removeAt(index)
            myAdapter.notifyItemRemoved(index)     }

        // send cards to study activity
        val studyCardButton = findViewById<Button>(R.id.Study)
        studyCardButton.setOnClickListener {

            val studyBun = Bundle()
            studyBun.putString("card1Question", myCards[0].question)
            studyBun.putString("card1Answer", myCards[0].answer)
            studyBun.putString("card2Question", myCards[1].question)
            studyBun.putString("card2Answer", myCards[1].answer)
            studyBun.putString("card3Question", myCards[2].question)
            studyBun.putString("card3Answer", myCards[2].answer)
            studyBun.putString("card4Question", myCards[3].question)
            studyBun.putString("card4Answer", myCards[3].answer)
            studyBun.putString("card5Question", myCards[4].question)
            studyBun.putString("card5Answer", myCards[4].answer)
            studyBun.putString("card6Question", myCards[5].question)
            studyBun.putString("card6Answer", myCards[5].answer)
            studyBun.putString("card7Question", myCards[6].question)
            studyBun.putString("card7Answer", myCards[6].answer)
            studyBun.putString("card8Question", myCards[7].question)
            studyBun.putString("card8Answer", myCards[7].answer)
            studyBun.putString("card9Question", myCards[8].question)
            studyBun.putString("card9Answer", myCards[8].answer)
            studyBun.putString("card10Question", myCards[9].question)
            studyBun.putString("card10Answer", myCards[9].answer)

            val studyIntent = Intent(this, StudySetActivity::class.java)
            studyIntent.putExtras(studyBun)
            startActivity(studyIntent)     }

        // return to main screen
        val doneCardButton = findViewById<Button>(R.id.firstDone)
        doneCardButton.setOnClickListener {

            val restartIntent = Intent(this, MainActivity::class.java)
            startActivity(restartIntent)     }


        // -----------------------------------------------------------------------------------------

        var finPosition = 0

        // get final card edits from dialog activity -----------------------------------------------
        val finalBun = intent.extras
        val fs1: String?
        val fs2: String?
        val fi1: Int?


        if (finalBun != null) {
            fi1 = finalBun.getInt("finalPosition", 0)
            finPosition = fi1 }

        if (finalBun != null) {
            fs1 = finalBun.getString("finalQuestion", myCards[finPosition].question)
            myCards[finPosition].question = fs1 }

        if (finalBun != null) {
            fs2 = finalBun.getString("finalAnswer", myCards[finPosition].answer)
            myCards[finPosition].answer = fs2    }    }


    // card functions to generate/add/remove -------------------------------------------------------
    private fun generateCards(size: Int): ArrayList<Flashcard> {
        val list = getHardcodedFlashcards()
        val arraylist = ArrayList(list)

        return arraylist     }      }