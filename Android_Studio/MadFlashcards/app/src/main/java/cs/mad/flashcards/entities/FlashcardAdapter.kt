package cs.mad.flashcards.entities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R


class FlashcardAdapter(private val cardList: List<Flashcard>,
                       private val listener: OnItemClickListener) :

        RecyclerView.Adapter<FlashcardAdapter.CardViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val cardView = LayoutInflater.from(parent.context).inflate(R.layout.flashcard,
            parent, false)

        return CardViewHolder(cardView) }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val currentItem = cardList[position]
        holder.textView1.text = currentItem.question
        holder.textView2.text = currentItem.answer }

    override fun getItemCount() = cardList.size

    inner class CardViewHolder(itemView: View) :
            RecyclerView.ViewHolder(itemView), View.OnClickListener {

        val textView1: TextView = itemView.findViewById(R.id.question)
        val textView2: TextView = itemView.findViewById(R.id.answer)

        private val clickToOpen: TextView = itemView.findViewById(R.id.clickToOpen)
        init { clickToOpen.setOnClickListener(this) }

        override fun onClick(v: View?) {
            var position = adapterPosition
            if (position != RecyclerView.NO_POSITION) { listener.onItemClick(position) } } }

    interface OnItemClickListener { fun onItemClick(position: Int) } }