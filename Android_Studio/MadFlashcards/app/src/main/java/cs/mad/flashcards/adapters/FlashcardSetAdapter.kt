package cs.mad.flashcards.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.databinding.ItemFlashcardSetBinding
import cs.mad.flashcards.entities.FlashDao
import cs.mad.flashcards.entities.FlashcardSet


class FlashcardSetAdapter(private var dataSet: List<FlashcardSet>, private val dao: FlashDao) :
    RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() {


    class ViewHolder(bind: ItemFlashcardSetBinding) : RecyclerView.ViewHolder(bind.root) {
        val binding: ItemFlashcardSetBinding = bind }


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        val binding = ItemFlashcardSetBinding.inflate(
            LayoutInflater.from(viewGroup.context), viewGroup, false)

        binding.root.minimumHeight = viewGroup.height / 4
        return ViewHolder(binding) }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.binding.flashcardSetTitle.text = item.title.toString()
        viewHolder.binding.root.setOnClickListener {
            viewHolder.itemView.context.startActivity(
                Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java)) } }


    override fun getItemCount(): Int { return dataSet.size }


    suspend fun addItem(it: String) {
        dao.addSet(FlashcardSet(11, it))
        notifyItemInserted(dataSet.lastIndex) }


    fun setData(items: List<FlashcardSet>) {
        dataSet = items
        notifyDataSetChanged() } }