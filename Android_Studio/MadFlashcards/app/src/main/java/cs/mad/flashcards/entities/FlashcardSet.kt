package cs.mad.flashcards.entities

import androidx.room.*


@Entity
data class FlashcardSet(

    @PrimaryKey(autoGenerate = true)
    val id: Int?, val title: String?   )


@Dao
interface FlashDao {

    @Query("SELECT * FROM FlashcardSet")
    suspend fun getSets(): List<FlashcardSet> {

        return mutableListOf(

                FlashcardSet(1, "Set 1"), FlashcardSet(2, "Set 2"),
                FlashcardSet(3, "Set 3"), FlashcardSet(4, "Set 4"),
                FlashcardSet(5, "Set 5"), FlashcardSet(6, "Set 6"),
                FlashcardSet(7, "Set 7"), FlashcardSet(8, "Set 8"),
                FlashcardSet(9, "Set 9"), FlashcardSet(10, "Set 10")) }

    @Delete
    suspend fun deleteSet(set: FlashcardSet)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addSet(set: FlashcardSet)  }


