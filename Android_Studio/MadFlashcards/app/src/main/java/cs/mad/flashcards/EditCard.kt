package cs.mad.flashcards

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class EditCard : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_card)


        val doneButton = findViewById<Button>(R.id.done_button)
        doneButton.setOnClickListener {


            val questionTextEdit = findViewById<EditText>(R.id.editText3)
            val answerTextEdit = findViewById<EditText>(R.id.editText4)

            val editedBun = Bundle()
            editedBun.putString("editedQuestion", questionTextEdit.text.toString())
            editedBun.putString("editedAnswer", answerTextEdit.text.toString())

            val reIntent = Intent(this, FlashCardDialog::class.java)
            reIntent.putExtras(editedBun)

            startActivity(reIntent)     }    }    }
