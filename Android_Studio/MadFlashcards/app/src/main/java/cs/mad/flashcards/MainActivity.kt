package cs.mad.flashcards

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.entities.*


class MainActivity : AppCompatActivity(), FlashcardSetAdapter.OnItemClickListener {

    private val mySets = generateSets(200)
    private val myAdapter = FlashcardSetAdapter(mySets, this)

    override fun onItemClick(position: Int) {

        Toast.makeText(this,"Set Opened", Toast.LENGTH_SHORT).show()
        myAdapter.notifyItemChanged(position)

        val intent = Intent(this, DetailedCardActivity::class.java)
        startActivity(intent) }


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_card_set)

        recyclerView.adapter = myAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)   }


    private fun generateSets(size: Int): ArrayList<FlashcardSet> {
        val list = getHardcodedFlashcardSets()
        val arraylist = ArrayList(list)

        return arraylist   } }