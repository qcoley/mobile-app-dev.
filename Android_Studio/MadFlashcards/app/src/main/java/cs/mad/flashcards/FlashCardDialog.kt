package cs.mad.flashcards

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView


class FlashCardDialog : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flash_card_dialog)

        val question: TextView = findViewById(R.id.questionView)
        val answer: TextView = findViewById(R.id.answerView)
        var position = 0


        // get values from original card in recycler view ------------------------------------------
        val originBun = intent.extras
        val os1: String?
        val os2: String?
        val oi1: Int?

        if (originBun != null) {
            os1 = originBun.getString("originQuestion", "Default")
            question.text = os1 }

        if (originBun != null) {
            os2 = originBun.getString("originAnswer", "Default")
            answer.text = os2 }

        if (originBun != null) {
            oi1 = originBun.getInt("originPosition", 0)
            position = oi1}

        val editButton = findViewById<Button>(R.id.edit_button)
        editButton.setOnClickListener{

            val intent = Intent(this, EditCard::class.java)
            startActivity(intent) }


        // get values from edit card activity ------------------------------------------------------
        val editedBun = intent.extras
        val es1: String?
        val es2: String?

        if (editedBun != null) {
            es1 = editedBun.getString("editedQuestion", question.text.toString())
            question.text = es1 }

        if (editedBun != null) {
            es2 = editedBun.getString("editedAnswer", answer.text.toString())
            answer.text = es2 }


        // send values back to original cards in recycler view -------------------------------------
        val soDoneButton = findViewById<Button>(R.id.so_done_button)
        soDoneButton.setOnClickListener {

            val backBun = Bundle()
            backBun.putString("finalQuestion", question.text.toString())
            backBun.putString("finalAnswer", answer.text.toString())
            backBun.putInt("finalPosition", position)

            val soIntent = Intent(this, DetailedCardActivity::class.java)
            soIntent.putExtras(backBun)

            startActivity(soIntent)   }   }   }


