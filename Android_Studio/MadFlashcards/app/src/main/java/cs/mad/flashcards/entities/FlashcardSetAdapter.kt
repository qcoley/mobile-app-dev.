package cs.mad.flashcards.entities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R


class FlashcardSetAdapter(private val setList: List<FlashcardSet>,
                          private val listener: OnItemClickListener) :

        RecyclerView.Adapter<FlashcardSetAdapter.SetViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SetViewHolder {
        val setView = LayoutInflater.from(parent.context).inflate(R.layout.flashcardset,
                parent, false)

        return SetViewHolder(setView) }

    override fun onBindViewHolder(holder: SetViewHolder, position: Int) {
        val currentItem = setList[position]
        holder.textView1.text = currentItem.title
        holder.textView2.text = currentItem.title}

    override fun getItemCount() = setList.size

    inner class SetViewHolder(itemView: View) :
            RecyclerView.ViewHolder(itemView), View.OnClickListener{

        val textView1: TextView = itemView.findViewById(R.id.button)
        val textView2: TextView = itemView.findViewById(R.id.button2)

        init { textView1.setOnClickListener(this) }
        init { textView2.setOnClickListener(this) }

        override fun onClick(v: View?) {
            var position = adapterPosition
            if (position != RecyclerView.NO_POSITION) { listener.onItemClick(position) } } }

    interface OnItemClickListener { fun onItemClick(position: Int) } }