package cs.mad.flashcards

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cs.mad.flashcards.entities.FlashDao
import cs.mad.flashcards.entities.FlashcardSet


@Database(entities = [FlashcardSet::class], version = 1, exportSchema = false)


abstract class FlashDatabase : RoomDatabase() {

    abstract fun FlashDao(): FlashDao

    companion object {

        @Volatile
        private var INSTANCE: FlashDatabase? = null

        fun getSetDatabase(context: Context): FlashDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder( context.applicationContext,
                    FlashDatabase::class.java, "FlashcardSet_database").build()

                INSTANCE = instance

                instance } } } }